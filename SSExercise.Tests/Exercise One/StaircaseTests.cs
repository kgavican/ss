﻿using SSExercise.Exercise_One;
using Xunit;

namespace SSExercise.Tests.Exercise_One
{
    public class StaircaseTests
    {
        private readonly StrideCalculator _strideCalculator = new StrideCalculator();

        [Fact]
        public void StaircaseCaseOneIsCalculatedCorrectly()
        {
            // Arrange 
            int[] flights = {15};
            int stepsPerStride = 2;
            int expectedResult = 8;

            // Act 
            var result = _strideCalculator.CalculateForStairs(flights, stepsPerStride, 2);

            // Assert
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void StaircaseCaseTwoIsCalculatedCorrectly()
        {
            // Arrange 
            int[] flights = { 15,15 };
            int stepsPerStride = 2;
            int expectedResult = 18;

            // Act 
            var result = _strideCalculator.CalculateForStairs(flights, stepsPerStride, 2);

            // Assert
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void StaircaseCaseThreeIsCalculatedCorrectly()
        {
            // Arrange
            int[] flights = {5, 11, 9, 13, 8, 30, 14};
            int stepsPerStride = 3;
            int expectedResult = 44;

            // Act 
            var result = _strideCalculator.CalculateForStairs(flights, stepsPerStride, 2);

            // Assert
            Assert.Equal(expectedResult, result);
        }
    }
}
