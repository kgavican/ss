﻿using System.Numerics;
using SSExercise.ExerciseThree;
using Xunit;

namespace SSExercise.Tests.Exercise_Three
{
    public class RecursiveAdderTests
    {
        /*
         * [Test]
            [TestCaseSource("Add_Source")]
            public AddResult Add_UsingARecursiveAlgorithm_ValuesAreAdded(byte[] f, byte[] s)
            {
                // Arrange

                // Act
                var result = AddRecursive(f, s);

                // Assert
                return new AddResult(f, s, result);
            }

            E.G.
            Input : { 1, 1, 1 }, { 1, 1, 1 }
            Result: {2,2,2}

            Input : { 1, 1, 255 }, {0, 0, 1 }
            Result: {1,2,0}


            Conditions:
You can assume inputs f & s are never null, and are always of the same length. 
The algorithm should be non-destructive to the inputs.
The algorithm should be able to handle large input lengths, of a couple of thousand values, but the input will never be large enough to cause a stack overflow.

         */
    
        [Fact]
        public void Add_UsingARecursiveAlgorithm_ValuesAreAdded_Case1()
        {
            // Arrange
            byte[] f = { 1, 1, 1 };
            byte[] s = { 1, 1, 1 };

            // Act
            byte[] result = RecursiveAdder.AddBytes(f, s);

            // Assert
            Assert.Equal(2, result[0]);
            Assert.Equal(2, result[1]);
            Assert.Equal(2, result[2]);
        }


        [Fact]
        public void Add_UsingARecursiveAlgorithm_ValuesAreAdded_Case2()
        {
            // Arrange
            byte[] f = { 1, 1, 255 };
            byte[] s = { 0, 0, 1 };

            // Act
            byte[] result = RecursiveAdder.AddBytes(f, s);

            // Assert
            Assert.Equal(1, result[0]);
            Assert.Equal(2, result[1]);
            Assert.Equal(0, result[2]);
        }

        /*
        [Fact]
        public void ProofForTestCaseTwo()
        {
            // Arrange
            byte[] f = { 1, 1, 255 };
            byte[] s = { 0, 0, 1 };
            byte[] r = { 1, 2, 0 };

            var fNum = new BigInteger(f);
            var sNum = new BigInteger(s);
            var rNum = new BigInteger(r);

            // Act
            var result = BigInteger.Add(fNum, sNum).ToByteArray();

            // Assert
            Assert.Equal(1, result[0]);
            Assert.Equal(2, result[1]);
            Assert.Equal(0, result[2]);
        }
        */
    }
}
