﻿using System.Linq;
using SSExercise.ExerciseTwo;
using Xunit;

namespace SSExercise.Tests.Exercise_Two
{
    public class BombAvoiderTests
    {
        /*
         Before the bombs explode (all simultaneously), you have just enough time to
         travel to any integer point in the cube [0, 0, 0]-[1000, 1000, 1000], both inclusive.
         you must find the point with the maximum distance to the nearest bomb, which your captain's
         intuition tells you will be the safest 
         */

        // DOESN"T WORK!

        [Fact]
        public void RunBombs()
        {
            // Arrange
            var avoider = new BombAvoider();

            // Act
            var tests = TestData.GenerateBombAttacks();

            var result = avoider.FindSafestPoint(tests.First());

            // Assert
            Assert.Equal(500, result.X);
            Assert.Equal(500, result.Y);
            Assert.Equal(500, result.Z);
        }


        [Fact]
        public void FurthestDistnaceFromCornerByZJump()
        {
            var avoider = new BombAvoider();

            var expectedResult = new Point(0, 1000, 1000);

            var currentPoint = new Point(1000, 0, 0);

            //var stopWatch = new Stopwatch();
            //stopWatch.Start();

            var result = avoider.GetFurthestPointByZJump(currentPoint);

            //stopWatch.Stop();

            Assert.Equal(expectedResult.X, result.X);
            Assert.Equal(expectedResult.Y, result.Y);
            Assert.Equal(expectedResult.Z, result.Z);
        }

        [Fact]
        public void MidPointIsCalculated()
        {
            // arrange
            var expectedResult = new Point(500,500,500);

            // act
            var result = JumpCalculations.GetMidPoint(new Point(0, 1000, 1000), new Point(1000, 0, 0));

            // assert
            Assert.Equal(expectedResult.X, result.X);
            Assert.Equal(expectedResult.Y, result.Y);
            Assert.Equal(expectedResult.Z, result.Z);
        }

    }
}
