﻿using System.Collections.Generic;
using SSExercise.ExerciseTwo;

namespace SSExercise.Tests.Exercise_Two
{
    public static class TestData
    {
        public static List<BombAttack> GenerateBombAttacks()
        {
            var list = new List<BombAttack>
            {
                new BombAttack()
                {
                    NumberOfBombs = 8,
                    BombPositions = new List<Point>()
                    {
                        new Point(0, 0, 0),
                        new Point(0, 0, 1000),
                        new Point(0, 1000, 0),
                        new Point(0, 1000, 1000),
                        new Point(1000, 0, 0),
                        new Point(1000, 0, 1000),
                        new Point(1000, 1000, 0),
                        new Point(1000, 1000, 1000)
                    }
                }
            };

            return list;
        }
    }
}
