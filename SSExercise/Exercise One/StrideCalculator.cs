﻿using System;
using System.Linq;

namespace SSExercise.Exercise_One
{
    public class StrideCalculator
    {
        public int CalculateForStairs(int[] flights, int stepsPerStride, int stridesPerLanding)
        {
            if (flights == null) throw new ArgumentNullException(nameof(flights));

            var totalStrides = flights.Sum(t => CalcStridesForFlightOfStairs(t, stepsPerStride));

            totalStrides += (flights.Length - 1) * stridesPerLanding;

            return totalStrides;
        }


        private static int CalcStridesForFlightOfStairs(int stepsPerFlight, int stepsPerStride)
        {
            return stepsPerFlight % stepsPerStride == 0
                ? stepsPerFlight / stepsPerStride
                : (stepsPerFlight / stepsPerStride) + 1;
        }
    }
}
