﻿using System;
using System.Linq;

namespace SSExercise.ExerciseThree
{
    public static class RecursiveAdder
    {
        /// <summary> Adds 2 byte arrays together. </summary>
        /// <param name="f">First array.</param>
        /// <param name="s">Second array.</param>
        /// <returns>Result array containing the sum of the two.</returns>
        public static byte[] AddBytes(byte[] f, byte[] s)
        {
            byte[] result = new byte[f.Length];

            int startAt = f.Length - 1;

            return Add(f, s, startAt, result);
        }

        private static byte[] Add(byte[] f, byte[] s, int index, byte[] result, int carry = 0)
        {
            if (index < 0)
            {
                if (carry == 1)
                {
                    var resultBytes = result.ToList();

                    resultBytes.Insert(0, 1);

                    return resultBytes.ToArray();
                }

                return result;
            }

            int remainder;

            int sum = f[index] + s[index] + carry;

            carry = Math.DivRem(sum, 256, out remainder);

            result[index] = (byte)remainder;

            return Add(f, s, index - 1, result, carry);
        }
    }
}
