﻿using System.Collections.Generic;

namespace SSExercise.ExerciseTwo
{
    public class BombAttack
    {
        public int NumberOfBombs { get; set; }

        public List<Point> BombPositions { get; set; } = new List<Point>();
    }
}
