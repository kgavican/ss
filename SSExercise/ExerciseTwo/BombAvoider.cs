﻿using System;

namespace SSExercise.ExerciseTwo
{
    public class BombAvoider
    {
        private readonly int _cubeSize;
        private readonly int _midPoint;

        public BombAvoider(int cubeSize=1000)
        {
            _cubeSize = cubeSize;
            _midPoint = _cubeSize / 2;
        }


        public Point FindSafestPoint(BombAttack attack)
        {
            // Dont know how to tackle this one I'm afraid....

            // Probably need to treat as cluster

            // Maybe give the bombs a radius and see where they don't intersect
            
            // Calculate and print the square of distance to 
            // the nearest bomb from the safest point in the cube.

            throw new NotImplementedException();
        }


        public Point GetFurthestPointByZJump(Point bombPoint)
        {
            int zPoint = 0;

            // Check Z value
            if (bombPoint.Z < _midPoint)
            {
                zPoint = _cubeSize;
            }

            var goToPoint = new Point();
            int boundary = _cubeSize + 1;
            int distance = 0;

            for (int x = 0; x < boundary; x++)
            {
                for (int y = 0; y < boundary; y++)
                {
                    var point = new Point(x, y, zPoint);

                    var d = JumpCalculations.CalcSquareDistance(bombPoint, point);

                    if (d <= distance) continue;

                    goToPoint = point;

                    distance = d;
                }
            }

            return goToPoint;
        }


    }
}
