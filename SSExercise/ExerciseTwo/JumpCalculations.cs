﻿using System;

namespace SSExercise.ExerciseTwo
{
    public static class JumpCalculations
    {
        public static int CalcSquareDistance(Point point1, Point point2)
        {
            var dSquared = Math.Pow(point2.X - point1.X, 2) + Math.Pow(point2.Y - point1.Y, 2) +
                           Math.Pow(point2.Z - point1.Z, 2);

            return (int)dSquared;
        }

        public static Point GetMidPoint(Point point1, Point point2)
        {
            return new Point(((point1.X + point2.X)/ 2), ((point1.Y + point2.Y) / 2), ((point1.Z + point2.Z) / 2));
        }
    }
}
